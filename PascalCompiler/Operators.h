#pragma once
#include "stdafx.h"
#include <map>
#include <string>
#include "Types.h"

using namespace std;

static map<string, Operator> operatorDict = {
{ "*", star },
{ "/", slash },
{ "=", equall },
{ ",", comma },
{ ";", semicolon },
{ ":", colon },
{ ".",point },
{ "^", arrow },
{ "(",leftpar },
{ ")", rightpar },
{ "[", lbracket },
{ "]", rbracket },
{ "{", flpar },
{ "}",frpar },
{ "<", later },
{ ">", greater },
{ "+", pluss },
{ "-", minuss },
{"(*", lcomment},
{"*)", rcomment},
{":=", assign}
};
