#pragma once
#include <string>
#include "Operators.h"
#include "Symbol.h"
#include "Lexer.h"
#include "Context.h"

using namespace std;

class Compiler
{
private:
	Symbol * _currentSymbol;
	Lexer* _lexer;
	Context* _context;
public:
	Compiler();
	~Compiler();
	void Compile(string textProgram);
	void accept(tSymbolType expectedType, Operator expectedOperator);
	void accept(tSymbolType expectedType);
	void accept(tSymbolType expectedType, Keyword expectedKeyWord);
	void handle();
	void program();
	void varpart();
	void vardeclaration();
	void statementpart();
	void block();
	void operatorPascal();
	void variable();
	void simpleConstant();
	void simpleExpression();
	void slagaemoe();
	void mnozhitel();
};

