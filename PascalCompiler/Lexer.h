#pragma once
#include <string>;
#include <vector>;
#include "Symbol.h"
#define MAXIDENT 9 /*������������ ����� ��������� �����*/
#define MAXLEN 80 /*������������ ����� ������*/
#define MAXERR 900 /* ������������ ���������� ������ */
#define EOF -1 //����� ����� (���� � ���, ��� �� ���������)

struct Error;
struct TextPosition;
using namespace std;

class Lexer
{
	char _currentChar;
	TextPosition *_position;
	string _textProgram;
	string _currentLine;
	int _charsCount;
	vector<string> _lines;
	Error* _errors[MAXERR];

	bool readNewLine();
	void printLine();
	void addError(unsigned errorCode, TextPosition position);
	void printErrors();
	void nextCh();
	Symbol* getOperator();
	
public:
	Lexer();
	Lexer(string textProgram);
	~Lexer();
	Symbol* nextSymbol();
};

struct TextPosition
{
	int LineNumber;
	int CharNumber;
};

struct Error 
{
	unsigned errorCode;
	TextPosition position;
};