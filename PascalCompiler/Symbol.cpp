#include "stdafx.h"
#include "Symbol.h"
#include "Operators.h"
#include <iostream>
#include "Keywords.h"
#include "IntVariable.h"
#include "RealVariable.h"
#include "BoolVariable.h"
#include "CharVariable.h"


Symbol::Symbol()
{
}


Symbol::~Symbol()
{
}

void Symbol::print()
{
	switch (Type)
	{
	case TOperator:
		{
		string oper;
		for (auto &i : operatorDict) {
			if (i.second == this->Operator) {
				oper = i.first;
				break;
			}
		}
		printf("ln %d, col %d, operator \"%s\" \n", LineNumber, CharNumber, oper.c_str());
		}break;
	case TIdentificator:
		{
			printf("ln %d, col %d, identificator \"%s\" \n", LineNumber, CharNumber, Identificator.c_str());
		}break;
	case TKeyword:
		{
		string keyword;
		for (auto &i : keywordDict) {
			if (i.second == this->Keyword) {
				keyword = i.first;
				break;
			}
		}
		printf("ln %d, col %d, keyword \"%s\" \n", LineNumber, CharNumber, keyword.c_str());
		} break;
	case TConst:
		{
			switch(Variable->Type) 
			{ 
			case Int:
				{
				IntVariable* variable = dynamic_cast<IntVariable*>(Variable);
				printf("ln %d, col %d, integer const: %d \n", LineNumber, CharNumber, variable->Value);
				} break;
			case Real:
				{
				RealVariable* variable = dynamic_cast<RealVariable*>(Variable);
				printf("ln %d, col %d, real const: %f \n", LineNumber, CharNumber, variable->Value);
				} break;
			case Bool:
				{
				BoolVariable* variable = dynamic_cast<BoolVariable*>(Variable);
				printf("ln %d, col %d, bool const: %s \n", LineNumber, CharNumber, variable->Value ? "true" : "false");
				} break;
			case Char:
				{
				CharVariable* variable = dynamic_cast<CharVariable*>(Variable);
				printf("ln %d, col %d, char const: %c \n", LineNumber, CharNumber, variable->Value);
				} break;
			default: ;
			}
} break;
	default: ;
	}
}
