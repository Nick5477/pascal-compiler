#pragma once
#include "Types.h";
#include "Variable.h"

using namespace std;

class Symbol
{
public:
	Symbol();
	~Symbol();
	SymbolType Type;
	int LineNumber;
	int CharNumber;
	union {
		Operator Operator; /*��������*/
		Keyword Keyword; /*�������� �����*/
		Variable* Variable; /*���������*/
		string Identificator;/*�������������*/
	};
	void print();
};

