#pragma once
#include "Types.h"

class CType
{
public:
	CType();
	CType(ConstType type);
	~CType();
	ConstType Type;
};

