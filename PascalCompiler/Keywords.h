#pragma once
#include "stdafx.h"
#include <map>
#include <string>
#include "Types.h"

using namespace std;

static map<string, Keyword> keywordDict = {
{ "case", casesy },
{ "else", elsesy },
{ "file", filesy },
{ "goto", gotosy },
{ "then", thensy },
{ "type", typesy },
{ "until", untilsy },
{ "do", dosy },
{ "with", withsy },
{ "if", ifsy },
{ "of", ofsy },
{ "or", orsy },
{ "in", insy },
{ "to", tosy },
{ "end", endsy },
{ "var", varsy },
{ "div", divsy },
{ "and", andsy },
{ "not", notsy },
{ "for", forsy },
{ "mod", modsy },
{ "nil", nilsy },
{ "set", setsy },
{ "begin", beginsy },
{ "while", whilesy },
{ "array", arraysy },
{ "const", constsy },
{ "label", labelsy },
{ "downto", downtosy },
{ "packed", packedsy },
{ "record", recordsy },
{ "repeat", repeatsy },
{ "program", programsy },
{ "function", functionsy },
{ "procedure", proceduresy },
};