#pragma once
#include "Variable.h"
class RealVariable :
	public Variable
{
public:
	RealVariable();
	RealVariable(float val);
	~RealVariable();

	float Value;
};

