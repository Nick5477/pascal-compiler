#pragma once

#include "stdafx.h"
#include <map>
#include <string>

using namespace std;

static map<string, bool> booleanDict = {
{"True", true},
{"False", false }
};