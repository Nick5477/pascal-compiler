// PascalCompiler.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <string>
#include "Compiler.h"
#include <fstream>

using namespace std;

string readText(char* fileName) {
	ifstream in(fileName);
	string s((istreambuf_iterator<char>(in)), istreambuf_iterator<char>());

	return s;
}

int main()
{
	char* filename = (char*)"C:\\Users\\User\\Documents\\Visual Studio 2017\\Projects\\Compiler\\Debug\\program1.txt";
	string textprogram = readText(filename);
	Compiler *compiler = new Compiler();
	compiler->Compile(textprogram);
    return 0;
}

