#include "stdafx.h"
#include "Lexer.h"
#include <sstream>
#include "Keywords.h"
#include "Consts.h"
#include "BoolVariable.h"
#include "RealVariable.h"
#include "IntVariable.h"
#include "Operators.h"
#include "CharVariable.h"

bool Lexer::readNewLine()
{
	if (++_position->LineNumber < _lines.size())
	{
		_position->CharNumber = 0;
		_currentLine = _lines[_position->LineNumber];
		_charsCount = _currentLine.size();
		_currentChar = _currentLine[0];
		return true;
	}
	else
	{
		printf("Lexer finished!");
		_position->CharNumber = EOF;
		return false;
	}
}

void Lexer::printLine()
{
	printf("line #%d: %s", _position->LineNumber, _currentLine.c_str());
}

void Lexer::nextCh()
{
	if (_position->CharNumber >= (_charsCount - 1)) 
	{
		printLine();
		if (!readNewLine()) 
		{
			_position->CharNumber = EOF;
			_currentChar = EOF;
		}
		else 
		{
			_currentChar = _currentLine[_position->CharNumber];
		}
	}
	else 
	{
		_currentChar = _currentLine[++_position->CharNumber];
	}
}

Lexer::Lexer()
{
}

Lexer::Lexer(string textProgram)
{
	_textProgram = textProgram;
	stringstream stream(_textProgram);
	string line;
	while(getline(stream, line, '\n'))
	{
		_lines.push_back(line + '\n');
	}
	_position = new TextPosition();
	_position->LineNumber = 0;
	_position->CharNumber = 0;
	_currentLine = _lines[_position->LineNumber];
	_charsCount = _currentLine.size();
	_currentChar = _currentLine[_position->CharNumber];
}


Lexer::~Lexer()
{
}

Symbol* Lexer::nextSymbol()
{
	Symbol* newSymbol = nullptr;
	//�� ����� �����
	if (_position->CharNumber != EOF)
	{
		//������� �������� ��������
		while (_currentChar == '\n' || _currentChar == ' ' || _currentChar == '\t')
		{
			nextCh();
		}

		newSymbol = new Symbol();
		newSymbol->CharNumber = _position->CharNumber;
		newSymbol->LineNumber = _position->LineNumber;

		char tempLittera;
		if (_currentChar >= 'A' && _currentChar <='Z' 
			|| _currentChar >= 'a' && _currentChar <= 'z')
		{
			tempLittera = _currentChar;
			_currentChar = 'A';
		}
		if (_currentChar >= '0' && _currentChar <= '9')
		{
			tempLittera = _currentChar;
			_currentChar = '0';
		}

		string ident;
		string oper;

		switch(_currentChar)
		{
		case 'A': {
			_currentChar = tempLittera;
			int lengthId = 0;
			ident = "";
			ident.append(1, _currentChar);
			nextCh();

			while ((_currentChar >= 'A' && _currentChar <= 'Z'
				|| _currentChar >= 'a' && _currentChar <= 'z'
				|| _currentChar >= '0' && _currentChar <= '9'
				|| _currentChar == '_')
				&& _position->CharNumber != EOF)
			{
				ident.append(1, _currentChar);
				nextCh();
				lengthId++;
			}

			if (lengthId > MAXIDENT)
			{
				newSymbol->Type = SymbolType::TIdentificator;
				newSymbol->Identificator = string(ident);
			}
			else
			{
				if (keywordDict.find(ident) == keywordDict.end())
				{
					//��������� �� ��������� ���������
					if (booleanDict.find(ident) == booleanDict.end())
					{
						newSymbol->Type = SymbolType::TIdentificator;
						newSymbol->Identificator = string(ident);
					}
					else
					{
						newSymbol->Type = SymbolType::TConst;
						newSymbol->Variable = new BoolVariable(booleanDict.find(ident)->second);
					}
				}
				else
				{
					newSymbol->Type = SymbolType::TKeyword;
					newSymbol->Keyword = keywordDict.find(ident)->second;
				}
			}
		}break;
		case '0': {
			_currentChar = tempLittera;
			string number = "";

			newSymbol->Type = SymbolType::TConst;

			while (_currentChar >= '0' && _currentChar <= '9')
			{
				number.push_back(_currentChar);
				nextCh();
			}

			if (_currentChar == '.')
			{
				number.push_back(_currentChar);
				nextCh();
				while (_currentChar >= '0' && _currentChar <= '9')
				{
					number.push_back(_currentChar);
					nextCh();
				}

				newSymbol->Variable = new RealVariable(stof(number, 0));
			}
			else
			{
				newSymbol->Variable = new IntVariable(stoi(number, 0));
			}
			

		}break;
		
		case '*': {
			newSymbol = getOperator();
		}break;
		case '/':
			newSymbol = getOperator();
			break;
		case '+': {
			newSymbol = getOperator();
		}break;
		case '-': {
			newSymbol = getOperator();
		}break;
		case '=': {
			newSymbol = getOperator();
		}break;
		case '.': {
			newSymbol = getOperator();
		}break;
		case ',': {
			newSymbol = getOperator();
		}break;
		case ';': {
			newSymbol = getOperator();
		}break;
		case ':': {
			oper.push_back(_currentChar);
			newSymbol->Type = SymbolType::TOperator;
			nextCh();
			if (_currentChar == '=')
			{
				oper.push_back(_currentChar);
			}
			newSymbol->Operator = operatorDict.find(oper)->second;
			nextCh();
		}break;
		case '\'': {
			nextCh();
			char val;
			newSymbol->Type = SymbolType::TConst;

			if (_currentChar == '\'')
			{
				nextCh();
				if (_currentChar == '\'')
				{
					nextCh();
					if (_currentChar == '\'')
					{
						newSymbol->Variable = new CharVariable('\'');
						nextCh();
					}
					else
					{
						newSymbol->Variable = new CharVariable('\1');
					}
				}
				else
				{
					newSymbol->Variable = new CharVariable('\0');
				}
			}
			else
			{
				val = _currentChar;
				nextCh();
				if (_currentChar == '\'')
				{
					newSymbol->Variable = new CharVariable(val);
					nextCh();
				}
				else
				{
					newSymbol->Variable = new CharVariable('\0');
				}
			}
		}break;
		case '(': {
			nextCh();
			if (_currentChar == '*')
			{
				do
				{
					do
					{
						nextCh();
					} while (_currentChar != '*' && _position->CharNumber != EOF);
					nextCh();
				} while (_currentChar != ')' && _position->CharNumber != EOF);
				nextCh();
				newSymbol = nextSymbol();
			}
			else
			{
				newSymbol->Type = SymbolType::TOperator;
				newSymbol->Operator = operatorDict.find("(")->second;
			}
		}break;
		case ')': {
			newSymbol = getOperator();
		}break;
		case '{': {
			do
			{
				nextCh();
			} while (_currentChar != '}' && _position->CharNumber != EOF);
			if (_currentChar == '}' && _position->CharNumber != EOF)
			{
				nextCh();
				newSymbol = nextSymbol();
			}
		}break;
		case EOF:{
			return nullptr;
		}
		default: {
			
		}break;
		}
	}

	return newSymbol;
}

Symbol* Lexer::getOperator()
{
	Symbol* newSymbol = new Symbol();
	newSymbol->CharNumber = _position->CharNumber;
	newSymbol->LineNumber = _position->LineNumber;
	string oper;
	newSymbol->Type = SymbolType::TOperator;
	oper.push_back(_currentChar);
	newSymbol->Operator = operatorDict.find(oper)->second;
	nextCh();
	return newSymbol;
}