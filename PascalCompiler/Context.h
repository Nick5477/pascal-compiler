#pragma once
#include "stdafx.h"
#include <vector>
#include "VariableName.h"

using namespace std;

class Context
{
public:
	Context();
	~Context();
	vector<VariableName> Variables;
};

