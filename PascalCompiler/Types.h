#pragma once
#include "stdafx.h"
#include <string>
#include <map>

using namespace std;

/*��� �������*/
typedef enum tSymbolType {
	TOperator, /* �������� */
	TIdentificator, /* ������������� */
	TKeyword, /* �������� ����� */
	TConst /* ��������� (�������� ������-�� ����) */
} SymbolType;

/*���������*/
typedef enum tOperator {
	star, /* * */
	slash, /* / */
	equall, /* = */
	comma, /* , */
	semicolon, /* ; */
	colon, /* : */
	point, /* . */
	arrow, /* ^ */
	leftpar, /* ( */
	rightpar, /* ) */
	lbracket, /* [ */
	rbracket, /* ] */
	flpar, /* { */
	frpar, /* } */
	later, /* < */
	greater, /* > */
	laterequal, /* <= */
	greaterequal, /* => */
	latergreater, /* <> */
	pluss, /* + */
	minuss, /* - */
	lcomment, /* (* */
	rcomment, /* *) */
	assign, /* := */
	twopoints, /* .. */
} Operator;

/*�������� �����*/
typedef enum tKeyword {
	casesy, /* case */
	elsesy, /* else */
	filesy, /* file */
	gotosy, /* goto */
	thensy, /* then */
	typesy, /* type */
	untilsy, /* until */
	dosy, /* do */
	withsy, /* with */
	ifsy, /* if */
	ofsy, /* of */
	orsy, /* or */
	insy, /* in */
	tosy, /* to */
	endsy, /* end */
	varsy, /* var */
	divsy, /* div */
	andsy, /* and */
	notsy, /* not */
	forsy, /* for */
	modsy, /* mod */
	nilsy, /* nil */
	setsy, /* set */
	beginsy, /* begin */
	whilesy, /* while */
	arraysy, /* array */
	constsy, /* const */
	labelsy, /* label */
	downtosy, /* downto */
	packedsy, /* packed */
	recordsy, /* record */
	repeatsy, /* repeat */
	programsy, /* program */
	functionsy, /* function */
	proceduresy, /* procedure */
} Keyword;

/* ���� �������� */
typedef enum tConstType {
	Int,/* integer */
	Real, /* real */
	Bool, /* boolean */
	Char, /* char */
} ConstType;

/* ������� ����� */
static map<string, ConstType> mapTypes = {
{ "integer" , Int },
{ "real" , Real },
{ "boolean", Bool },
{ "char", Char }
};