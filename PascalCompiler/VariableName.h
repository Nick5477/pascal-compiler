#pragma once
#include <string>
#include "CType.h"

using namespace std;

class VariableName
{
public:
	VariableName(std::string name, CType* type);
	string Name;
	CType* Type;
	VariableName();
	~VariableName();
	void toString();
};

