#include "stdafx.h"
#include "Compiler.h"
#include <iostream>
#include "Lexer.h"
#include "Symbol.h"
#include "Keywords.h"
#include "CType.h";

Compiler::Compiler()
{
}


Compiler::~Compiler()
{
}

void Compiler::Compile(string textProgram)
{
	Lexer *lexer = new Lexer(textProgram);
	Symbol *symbol = nullptr;
	while ((symbol = lexer->nextSymbol()) != nullptr) 
	{
		symbol->print();
		symbol->~Symbol();
	}
	system("pause");
}

#pragma region Syntactic analysator

void Compiler::accept(tSymbolType expectedType, Operator expectedOperator) {
	_currentSymbol->print();
	if (_currentSymbol->Type == expectedType && _currentSymbol->Operator == expectedOperator) {
		_currentSymbol = _lexer->nextSymbol();
	}
	else {
		printf("Error\n");
	}
}

void Compiler::accept(tSymbolType expectedType) {
	_currentSymbol->print();
	if (_currentSymbol->Type == expectedType) {
		_currentSymbol = _lexer->nextSymbol();
	}
	else {
		printf("Error\n");
	}
}

void Compiler::accept(tSymbolType expectedType, Keyword expectedKeyWord) {
	_currentSymbol->print();
	if (_currentSymbol->Type == expectedType && _currentSymbol->Keyword == expectedKeyWord) {
		_currentSymbol = _lexer->nextSymbol();
	}
	else {
		printf("Error\n");
	}
}

void Compiler::handle() {
	program();
}

/* <���������>::=program<���>;<����>. */
void Compiler::program() {
	accept(tSymbolType::TKeyword, tKeyword::programsy);
	accept(tSymbolType::TIdentificator);
	accept(tSymbolType::TOperator, tOperator::semicolon);
	block();
	accept(tSymbolType::TOperator, tOperator::point);
}

/* <������ ����������>:= var <�������� ���������� ����������>;{<�������� ���������� ����������>;} | <�����> */
void Compiler::varpart() {
	if (_currentSymbol->Type == tSymbolType::TKeyword && _currentSymbol->Keyword == tKeyword::varsy) {
		accept(tSymbolType::TKeyword, tKeyword::varsy);
		do {
			vardeclaration();
			accept(tSymbolType::TOperator, tOperator::semicolon);
		} while (_currentSymbol->Type == tSymbolType::TIdentificator);
	}
}

/* <�������� ���������� ����������>:: = <���>{ ,<���> }:<���> */
void Compiler::vardeclaration() {
	vector<string> varNames = vector<string>();/* ������ ���� i-�� ���� ���������� */
	varNames.push_back(_currentSymbol->Identificator.c_str());
	accept(tSymbolType::TIdentificator);
	while (_currentSymbol->Type == tSymbolType::TOperator && _currentSymbol->Operator == tOperator::comma) {
		_currentSymbol = _lexer->nextSymbol();/* ������� ������� */
		varNames.push_back(_currentSymbol->Identificator);
		accept(tSymbolType::TIdentificator);
	}
	accept(tSymbolType::TOperator, tOperator::colon);
	//�� ����������� ������� ������ ��������� ���
	ConstType type = mapTypes.at(_currentSymbol->Identificator);
	for (auto &i : varNames) {
		VariableName* var = new VariableName(i, new CType(type));
		_context->Variables.push_back(*var);
	}
	accept(tSymbolType::TIdentificator);
}


void Compiler::statementpart() {
	accept(tSymbolType::TKeyword, tKeyword::beginsy);/* begin */

	while (_currentSymbol->Type != tSymbolType::TKeyword || _currentSymbol->Keyword != tKeyword::endsy)
	{
		operatorPascal();/* �������� */
		if (_currentSymbol->Type == tSymbolType::TOperator && _currentSymbol->Operator == tOperator::semicolon) {
			accept(tSymbolType::TOperator, tOperator::semicolon);
		}
		else {
			/* ����� */
		}
	}
	accept(tSymbolType::TKeyword, tKeyword::endsy);/* end */
}

/* <����>::=<������ �����><������ ��������> <������ �����><������ ����������> <������ �������� � �������><������ ����������> */
void Compiler::block() {
	varpart();
	statementpart();
}

void Compiler::operatorPascal() {
	/* begin ��������; end */
	if (_currentSymbol->Type == tSymbolType::TKeyword && _currentSymbol->Keyword == tKeyword::beginsy) {
		accept(tSymbolType::TKeyword, tKeyword::beginsy);

		while (_currentSymbol->Type != tSymbolType::TKeyword || _currentSymbol->Keyword != tKeyword::endsy) {
			operatorPascal();/* �������� */
			accept(tSymbolType::TOperator, Operator::semicolon);
		}

		accept(tSymbolType::TKeyword, tKeyword::endsy);
	}
	/* ���������� */
	if (_currentSymbol->Type == tSymbolType::TIdentificator) {
		variable();
		accept(tSymbolType::TOperator, Operator::assign);
		simpleExpression();
	}
}

void Compiler::variable() {
	accept(tSymbolType::TIdentificator);
	/* ��� ��� ����� ��� ������ ����� */
}

void Compiler::simpleConstant() {
	switch (_currentSymbol->Type)
	{
	case tSymbolType::TIdentificator: {/* ��� ��������� */
		accept(tSymbolType::TIdentificator);
	}break;

	case tSymbolType::TConst: {/* ����� ��� ����� ��� char */
							  /* �������� �������� �� char ���������� */
		if (_currentSymbol->Variable->Type == tConstType::Char) {
			accept(tSymbolType::TConst);
		}
		/* �������� �������� �� ������ ��� ����� */
		if (_currentSymbol->Variable->Type == tConstType::Int) {
			accept(tSymbolType::TConst);
		}
	}break;

	default:
		break;
	}
}

void Compiler::simpleExpression() {
	/* +- ��������� + - or ��������� */
	if (_currentSymbol->Type == tSymbolType::TOperator &&
		(_currentSymbol->Operator == tOperator::pluss || _currentSymbol->Operator == tOperator::minuss)) {
		if (_currentSymbol->Operator == tOperator::pluss) {
			accept(tSymbolType::TOperator, tOperator::pluss);
		}
		else {
			accept(tSymbolType::TOperator, tOperator::minuss);
		}
	}
	slagaemoe();
	while (_currentSymbol->Type == tSymbolType::TOperator &&
		(_currentSymbol->Operator == tOperator::pluss || _currentSymbol->Operator == tOperator::minuss))
	{
		if (_currentSymbol->Operator == tOperator::pluss) {
			accept(tSymbolType::TOperator, tOperator::pluss);
		}
		else {
			accept(tSymbolType::TOperator, tOperator::minuss);
		}
		slagaemoe();
	}
}

void Compiler::slagaemoe() {
	mnozhitel();
	while ((_currentSymbol->Type == tSymbolType::TOperator &&
		(_currentSymbol->Operator == tOperator::star || _currentSymbol->Operator == tOperator::slash)))
	{
		switch (_currentSymbol->Type)
		{
		case tSymbolType::TOperator: {
			if (_currentSymbol->Operator == tOperator::star) {
				accept(tSymbolType::TOperator, tOperator::star);
			}
			else {
				accept(tSymbolType::TOperator, tOperator::slash);
			}
		}break;

		default:
			break;
		}
		mnozhitel();
	}
}

void Compiler::mnozhitel() {
	switch (_currentSymbol->Type)
	{
	case tSymbolType::TOperator: {/* ( ��������� ) */
		accept(tSymbolType::TOperator, tOperator::leftpar);
		simpleExpression();
		accept(tSymbolType::TOperator, tOperator::rightpar);
	}break;

	case tSymbolType::TIdentificator: {/* ���������� */
		variable();
	}break;

	case tSymbolType::TConst: {/* ��������� ��� ����� */
		simpleConstant();
	}break;

	default:
		break;
	}
}



#pragma endregion 