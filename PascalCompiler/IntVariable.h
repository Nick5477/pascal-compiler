#pragma once
#include "Variable.h"

class IntVariable : public virtual Variable
{
public:
	IntVariable();
	IntVariable(int val);
	~IntVariable();
	int Value;
};

