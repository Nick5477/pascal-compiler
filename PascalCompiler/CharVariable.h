#pragma once
#include "Variable.h"
class CharVariable :
	public Variable
{
public:
	CharVariable();
	CharVariable(char val);
	~CharVariable();

	char Value;
};

