#pragma once
#include "Variable.h"
class BoolVariable :
	public Variable
{
public:
	BoolVariable();
	BoolVariable(bool val);
	~BoolVariable();

	bool Value;
};

